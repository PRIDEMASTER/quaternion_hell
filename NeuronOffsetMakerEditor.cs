﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(NeuronOffsetMaker))]
public class NeuronOffsetMakerEditor : Editor {

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        DrawDefaultInspector();
        NeuronOffsetMaker myScript = (NeuronOffsetMaker)target;
        if(GUILayout.Button("Make Offsets"))
        {
            myScript.generateOffsets();
        }
        if(GUILayout.Button("Apply Offsets"))
        {
            myScript.applyOffsets();
        }
    }
}
